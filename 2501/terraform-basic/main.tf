# Configure the GitLab Provider
provider "gitlab" {
  base_url = var.gitlab_url
  token    = var.gitlab_token
}
# Adding subgroup by group's full path
data "gitlab_group" "root" {
  full_path = "Root-2503"
}


# Configure Gitlab groups and projects
resource "gitlab_group" "wa_basic_group" {
  name        = "wa-${var.gitlab_group_number}"
  parent_id   = data.gitlab_group.root.id
  path        = "wa-${var.gitlab_group_number}"
  description = "A wa-${var.gitlab_group_number}-group"
  lfs_enabled = true
}
resource "gitlab_project" "wa_basic_project" {
  name         = "wa-${var.gitlab_group_number}-project"
  path         = "wa-${var.gitlab_group_number}-project"
  description  = "A wa-${var.gitlab_group_number}-project"
  namespace_id = gitlab_group.wa_basic_group.id
}
