# Provider vars
variable "gitlab_url" {
  description = "Gitlab server url"
  type        = string
  default     = "https://gitlab.com/api/v4"

}
variable "gitlab_token" {
  description = "Gitlab server token"
  type        = string
  default     = "glpat-K7YF5KA5xCPw_VvGyZes"

}
variable "gitlab_group_number" {
  description = "Gitlab group number"
  type        = number
}